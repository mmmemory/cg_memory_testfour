﻿using System;
using System.Collections.Generic;
using System.Text;

namespace prototype.Model
{
    public class DoublonCarte
    {
        private static int numero = 0;
        private Carte carte1 = null;
        private Carte carte2 = null;
        private bool status = false;
        private int identifiant = 0;


        public DoublonCarte(Carte laCarte)
        {
            numero++;
            this.identifiant = numero;
            this.carte1 = new Carte(laCarte);
            this.carte2 = new Carte(laCarte);
            this.carte1.SetDoublonCartes(this);
            this.carte2.SetDoublonCartes(this);
        }
        public Carte Getcarte1()
        {
            return this.carte1;
        }
        public Carte GetCarte2()
        {
            return this.carte2;
        }
        public bool GetStatus()
        {
            return this.status;
        }
        public int getIdentifiant()
        {
            return this.identifiant;
        }
        public void VerifierStatus()
        {
            if (this.carte1.Getstatus() == this.carte2.Getstatus())
            {
                this.status = true;
            }
            else
            {
                this.status = false;
            }
        }
        public string Description()
        {
            return "\nLa carte 1 est : " + this.carte1 + "\nLa carte 2 est : " + this.carte2 + "\nSon status est : " + this.status;
        }
    }
}
